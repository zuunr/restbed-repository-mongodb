# restbed-repository-mongodb

![Apache License](https://img.shields.io/static/v1.svg?label=License&message=Apache-2.0&color=%230af)
[![Maven Central](https://img.shields.io/maven-central/v/com.zuunr.restbed/restbed-repository-mongodb.svg?label=Maven%20Central&color=%230af)](https://search.maven.org/search?q=a:restbed-repository-mongodb)
[![Snyk](https://snyk-widget.herokuapp.com/badge/mvn/com.zuunr.restbed/restbed-repository-mongodb/badge.svg)](https://snyk.io/vuln/maven:com.zuunr.restbed%3Arestbed-repository-mongodb)

This is the reactive mongodb implementation for Restbed modules. It should also be considered the standard implementation of the Reactive repository interface in the restbed-core module.

## Supported tags

* [`1.0.0.M8-cf5f7e5`, (*cf5f7e5/pom.xml*)](https://bitbucket.org/zuunr/restbed-repository-mongodb/src/cf5f7e5/pom.xml)

## Usage

To use this module, make sure this project's maven artifact is added as a dependency in your module, replacing 1.0.0-abcdefg with a supported tag:

```xml
<dependency>
    <groupId>com.zuunr.restbed</groupId>
    <artifactId>restbed-repository-mongodb</artifactId>
    <version>1.0.0-abcdefg</version>
</dependency>
```

## Configuration

The module requires additional configuration, see src/test/resources/application.yml for a sample.

Remember to use environment variables for database access properties instead of the application.yml file in a non-test environment.

## Indexes

Database indexes must be created manually (for now).

## Integration tests

To run the integration tests, a mongodb runtime must be started. Issue the following docker command to start a mongodb server.

First create the shared docker network:

```shell
docker network create restbed_default
```

Then start the mongodb server:

```shell
docker-compose up
```

After that the ReactiveMongoRepositoryIT can be executed.
