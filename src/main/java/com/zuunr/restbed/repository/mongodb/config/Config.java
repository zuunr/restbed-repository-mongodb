/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.repository.mongodb.config;

import javax.validation.constraints.NotNull;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.validation.annotation.Validated;

/**
 * <p>Configuration for mongodb runtime.</p>
 *  
 * @author Mikael Ahlberg
 */
@ConfigurationProperties(prefix = "zuunr.repository.mongodb")
@Validated
public class Config {

    @NotNull(message = "You must set if every collection should be prefixed with the api name in the database (default: false)")
    private final Boolean prefixCollectionWithApiName;
    @NotNull(message = "You must set the default collection limit, used to limit responses in a get collection call (default: 25)")
    private final Integer defaultGetCollectionLimit;

    @ConstructorBinding
    public Config(
            Boolean prefixCollectionWithApiName,
            Integer defaultGetCollectionLimit) {
        this.prefixCollectionWithApiName = prefixCollectionWithApiName;
        this.defaultGetCollectionLimit = defaultGetCollectionLimit;
    }
    
    public boolean isPrefixCollectionWithApiName() {
        return prefixCollectionWithApiName;
    }

    public int getDefaultGetCollectionLimit() {
        return defaultGetCollectionLimit;
    }
}
