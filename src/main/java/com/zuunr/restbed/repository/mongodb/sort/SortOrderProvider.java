/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.repository.mongodb.sort;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Component;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;

/**
 * <p>The SortOrderProvider is responsible for translating sort order techniques 
 * used in API to a spring data friendly {@link Order} object.</p>
 * 
 * @author Mikael Ahlberg
 */
@Component
public class SortOrderProvider {

    /**
     * <p>Creates a list of {@link Order} objects given the input {@link JsonArray}
     * containing parameters and their ascending/descending order.</p>
     * 
     * @param sortOrder is a {@link JsonArray} containing the parameter sort order
     * @return a new list containing Order objects
     */
    public List<Order> getSortOrder(JsonArray sortOrder) {
        List<Order> orders = new ArrayList<>();

        for (JsonObject orderObject : sortOrder.asList(JsonObject.class)) {
            String field = orderObject.get("param").getValue(String.class);
            boolean ascending = orderObject.get("asc", JsonValue.TRUE).getValue(Boolean.class);

            if (ascending) {
                orders.add(Order.asc(field));
            } else {
                orders.add(Order.desc(field));
            }
        }

        return orders;
    }
}
