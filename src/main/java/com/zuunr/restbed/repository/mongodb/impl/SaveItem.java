/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.repository.mongodb.impl;

import com.zuunr.json.JsonValue;
import com.zuunr.restbed.core.api.ApiUriInfo;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.stereotype.Component;

import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectSupport;
import com.zuunr.restbed.core.exchange.Exchange;
import com.zuunr.restbed.core.exchange.Response;
import com.zuunr.restbed.core.exchange.StatusCode;
import com.zuunr.restbed.core.service.util.EtagProvider;
import com.zuunr.restbed.repository.ReactiveRepository;
import com.zuunr.restbed.repository.mongodb.util.CollectionNameProvider;
import com.zuunr.restbed.repository.mongodb.util.ResourceDeserializer;

import reactor.core.publisher.Mono;

/**
 * <p>Primary saveItem implementation of {@link ReactiveRepository}.</p>
 * 
 * @see ReactiveRepository
 */
@Component
public class SaveItem<T extends JsonObjectSupport> {

    private final Logger logger = LoggerFactory.getLogger(SaveItem.class);

    private static final String ITEM_ID_PARAM = "id";
    private static final String MONGO_INTERNAL_ID_PARAM = "_id";

    private ReactiveMongoTemplate reactiveMongoTemplate;
    private EtagProvider etagProvider;
    private CollectionNameProvider collectionNameProvider;
    private ResourceDeserializer resourceDeserializer;

    @Autowired
    public SaveItem(
            ReactiveMongoTemplate reactiveMongoTemplate,
            EtagProvider etagProvider,
            CollectionNameProvider collectionNameProvider,
            ResourceDeserializer resourceDeserializer) {
        this.reactiveMongoTemplate = reactiveMongoTemplate;
        this.etagProvider = etagProvider;
        this.collectionNameProvider = collectionNameProvider;
        this.resourceDeserializer = resourceDeserializer;
    }

    /**
     * @see ReactiveRepository#saveItem(Exchange, Class)
     */
    public Mono<Exchange<T>> saveItem(Exchange<T> exchange, Class<T> resourceClass) {
        String collectionName = collectionNameProvider.getCollectionName(exchange.getRequest());

        JsonObject body = exchange.getRequest().getBodyAsJsonObject();
        JsonObject bodyWithEtag = etagProvider.appendEtag(body);

        return reactiveMongoTemplate.insert(createDocumentFromBody(bodyWithEtag), collectionName)
                .flatMap(o -> successSave(exchange, o, resourceClass));
    }

    private Mono<Exchange<T>> successSave(Exchange<T> exchange, Document savedDocument, Class<T> resourceClass) {
        savedDocument.remove(MONGO_INTERNAL_ID_PARAM);
        T body = resourceDeserializer.deserialize(savedDocument, resourceClass);

        if (logger.isDebugEnabled()) {
            logger.debug("Successfully saved item: {}", exchange.getRequest().getUrl());
        }

        return Mono.just(exchange
                .response(Response.<T>create(StatusCode._201_CREATED)
                        .body(body)));
    }

    private Document createDocumentFromBody(JsonObject body) {
        ApiUriInfo self = body.get("href", JsonValue.NULL).as(ApiUriInfo.class);
        
        String coreId;
        if (self == null) {
            coreId = ApiUriInfo.coreIdOf(body.get(ITEM_ID_PARAM).getString());
        } else {
            coreId = self.coreId();
        }
        
        JsonObject bodyWithId = body.put(MONGO_INTERNAL_ID_PARAM, coreId);
        return new Document(bodyWithId.asMapsAndLists()).append(MONGO_INTERNAL_ID_PARAM, new ObjectId(coreId));
    }
}
