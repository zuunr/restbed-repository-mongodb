/*
 * Copyright 2019 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.repository.mongodb.filter;

import java.util.regex.MatchResult;
import java.util.regex.Pattern;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Component;

/**
 * <p>The RangeCriteriaCreator is responsible for creating and
 * setting up a Criteria for range filtering, e.g. x &gt;= and x &lt; 20.</p>
 *
 * @author Mikael Ahlberg
 */
@Component
public class RangeCriteriaCreator {
    
    public static final Pattern pattern = Pattern.compile("([\\[\\(])([^\\,]*),([^\\)\\]]*)([\\]\\)])");
    
    private static final String INCLUSIVE_START_TAG = "[";
    private static final String INCLUSIVE_END_TAG = "]";
    
    /**
     * <p>Create a new range criteria with {less than, greater than, less or equals than
     * and greater or equals than} the provided values in form of a {@link MatchResult}.</p>
     * 
     * @param matcher contains the filter expression
     * @param path is the value to apply the filter on
     * @return a new {@link Criteria} object
     */
    public Criteria createCriteria(MatchResult matcher, String path) {
        try {
            String start = matcher.group(1);
            boolean inclusiveStart = start.equals(INCLUSIVE_START_TAG);
            String end = matcher.group(4);
            boolean inclusiveEnd = end.equals(INCLUSIVE_END_TAG);

            String intervalStart = matcher.group(2);
            String intervalEnd = matcher.group(3);

            Criteria criteria = new Criteria(path);

            addIntervalStart(inclusiveStart, intervalStart, criteria);
            addIntervalEnd(inclusiveEnd, intervalEnd, criteria);

            return criteria;
        } catch (Exception e) {
            throw new ApplyFilterException("An error occured when creating new filter Criteria", e);
        }
    }

    private void addIntervalStart(boolean inclusiveStart, String intervalStart, Criteria criteria) {
        if (intervalStart.isEmpty()) {
            return;
        }

        if (inclusiveStart) {
            criteria.gte(intervalStart);
        } else {
            criteria.gt(intervalStart);
        }
    }

    private void addIntervalEnd(boolean inclusiveEnd, String intervalEnd, Criteria criteria) {
        if (intervalEnd.isEmpty()) {
            return;
        }

        if (inclusiveEnd) {
            criteria.lte(intervalEnd);
        } else {
            criteria.lt(intervalEnd);
        }
    }
}
