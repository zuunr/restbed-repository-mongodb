/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.repository.mongodb;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;

import com.zuunr.json.JsonObjectSupport;
import com.zuunr.restbed.core.exchange.Collection;
import com.zuunr.restbed.core.exchange.Exchange;
import com.zuunr.restbed.repository.ReactiveRepository;
import com.zuunr.restbed.repository.mongodb.impl.DeleteItem;
import com.zuunr.restbed.repository.mongodb.impl.GetCollection;
import com.zuunr.restbed.repository.mongodb.impl.GetItem;
import com.zuunr.restbed.repository.mongodb.impl.SaveItem;
import com.zuunr.restbed.repository.mongodb.impl.UpdateItem;

import reactor.core.publisher.Mono;

/**
 * <p>Primary implementation of {@link ReactiveRepository}.</p>
 * 
 * @see ReactiveRepository
 */
@EnableReactiveMongoRepositories
public abstract class ReactiveMongoRepository<T extends JsonObjectSupport> implements ReactiveRepository<T> {

    private final Logger logger = LoggerFactory.getLogger(ReactiveMongoRepository.class);

    private @Autowired SaveItem<T> saveItem;
    private @Autowired GetItem<T> getItem;
    private @Autowired UpdateItem<T> updateItem;
    private @Autowired DeleteItem<T> deleteItem;
    private @Autowired GetCollection<T> getCollection;

    @Override
    public Mono<Exchange<T>> saveItem(Exchange<T> exchange, Class<T> resourceClass) {
        if (logger.isDebugEnabled()) {
            logger.debug("Calling save item with exchange for resource: {}", resourceClass.getName());
        }

        return Mono.from(saveItem.saveItem(exchange, resourceClass));
    }

    @Override
    public Mono<Exchange<T>> getItem(Exchange<T> exchange, Class<T> resourceClass) {
        if (logger.isDebugEnabled()) {
            logger.debug("Calling get item with exchange for resource: {}", resourceClass.getName());
        }

        return Mono.from(getItem.getItem(exchange, resourceClass));
    }

    @Override
    public Mono<Exchange<T>> updateItem(Exchange<T> exchange, Class<T> resourceClass) {
        if (logger.isDebugEnabled()) {
            logger.debug("Calling update item with exchange for resource: {}", resourceClass.getName());
        }

        return Mono.from(updateItem.updateItem(exchange, resourceClass));
    }

    @Override
    public Mono<Exchange<T>> deleteItem(Exchange<T> exchange) {
        if (logger.isDebugEnabled()) {
            logger.debug("Calling delete item with exchange");
        }

        return Mono.from(deleteItem.deleteItem(exchange));
    }

    @Override
    public Mono<Exchange<Collection<T>>> getCollection(Exchange<Collection<T>> exchange, Class<T> resourceClass) {
        if (logger.isDebugEnabled()) {
            logger.debug("Calling get collection with exchange for resource: {}", resourceClass.getName());
        }

        return Mono.from(getCollection.getCollection(exchange, resourceClass));
    }
}
