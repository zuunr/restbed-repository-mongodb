/*
 * Copyright 2019 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.repository.mongodb.filter;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Component;

/**
 * <p>The FixedValueCriteriaCreator is responsible for creating and
 * setting up a Criteria for fixed value filtering, e.g. x = 42 or x = 100.</p>
 *
 * @author Mikael Ahlberg
 */
@Component
public class FixedValueCriteriaCreator {
    
    /**
     * <p>Create a new Criteria which is either equals to the provided
     * value, or by using an orOperator, can be any of the provided values.</p>
     * 
     * @param filterExpression contains the fixed expression values
     * @param path is the value to apply the filter on
     * @return a new {@link Criteria} object
     */
    public Criteria createCriteria(String filterExpression, String path) {
        if (filterExpression.contains(",")) {
            return createMultiFixedValueCriteria(filterExpression, path);
        }
        
        return Criteria.where(path)
                .is(filterExpression);
    }
    
    private Criteria createMultiFixedValueCriteria(String filterExpression, String path) {
        String[] fixedValues = filterExpression.split(",");
        Criteria[] criterias = new Criteria[fixedValues.length];
        
        for (int i = 0; i < fixedValues.length; i++) {
            criterias[i] = Criteria.where(path)
                    .is(fixedValues[i]);
        }
        
        return new Criteria()
                .orOperator(criterias);
    }
}
