/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.repository.mongodb.impl;

import java.util.List;
import java.util.Optional;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObjectSupport;
import com.zuunr.restbed.core.exchange.Collection;
import com.zuunr.restbed.core.exchange.Exchange;
import com.zuunr.restbed.core.exchange.Response;
import com.zuunr.restbed.core.exchange.StatusCode;
import com.zuunr.restbed.repository.ReactiveRepository;
import com.zuunr.restbed.repository.mongodb.config.Config;
import com.zuunr.restbed.repository.mongodb.filter.FilterQueryProvider;
import com.zuunr.restbed.repository.mongodb.sort.SortOrderProvider;
import com.zuunr.restbed.repository.mongodb.util.CollectionNameProvider;
import com.zuunr.restbed.repository.mongodb.util.ResourceDeserializer;

import reactor.core.publisher.Mono;

/**
 * <p>Primary getCollection implementation of {@link ReactiveRepository}.</p>
 * 
 * @see ReactiveRepository
 */
@Component
public class GetCollection<T extends JsonObjectSupport> {

    private final Logger logger = LoggerFactory.getLogger(GetCollection.class);

    private static final int DEFAULT_OFFSET = 0;
    private static final String MONGO_INTERNAL_ID_PARAM = "_id";

    private ReactiveMongoTemplate reactiveMongoTemplate;
    private CollectionNameProvider collectionNameProvider;
    private FilterQueryProvider filterQueryProvider;
    private SortOrderProvider sortOrderProvider;
    private Config config;
    private ResourceDeserializer resourceDeserializer;

    @Autowired
    public GetCollection(
            ReactiveMongoTemplate reactiveMongoTemplate,
            CollectionNameProvider collectionNameProvider,
            FilterQueryProvider filterQueryProvider,
            SortOrderProvider sortOrderProvider,
            Config config,
            ResourceDeserializer resourceDeserializer) {
        this.reactiveMongoTemplate = reactiveMongoTemplate;
        this.collectionNameProvider = collectionNameProvider;
        this.filterQueryProvider = filterQueryProvider;
        this.sortOrderProvider = sortOrderProvider;
        this.config = config;
        this.resourceDeserializer = resourceDeserializer;
    }

    /**
     * @see ReactiveRepository#getCollection(Exchange, Class)
     */
    public Mono<Exchange<Collection<T>>> getCollection(Exchange<Collection<T>> exchange, Class<T> resourceClass) {
        String collectionName = collectionNameProvider.getCollectionName(exchange.getRequest());

        JsonArray filter = exchange.getRequest().getApiUriInfo().filter();
        JsonArray sortOrder = exchange.getRequest().getApiUriInfo().sortOrder();

        int offset = getOffset(exchange);
        int limit = getLimit(exchange);

        Query query = filterQueryProvider.getFilterQuery(filter)
                .with(Sort.by(sortOrderProvider.getSortOrder(sortOrder)))
                .skip(offset)
                .limit(limit);

        return reactiveMongoTemplate.find(query, Document.class, collectionName)
                .flatMapSequential(o -> deserialize(o, resourceClass))
                .collectList()
                .map(o -> successGetCollection(exchange, o, offset, limit));
    }

    private Mono<T> deserialize(Document fetchedDocument, Class<T> resourceClass) {
        fetchedDocument.remove(MONGO_INTERNAL_ID_PARAM);

        return Mono.just(resourceDeserializer.deserialize(fetchedDocument, resourceClass));
    }

    private Exchange<Collection<T>> successGetCollection(Exchange<Collection<T>> exchange, List<T> fetchedItems, int offset, int limit) {
        Collection<T> collection = Collection.<T>builder()
                .offset(offset)
                .limit(limit)
                .value(fetchedItems)
                .href(exchange.getRequest().getApiUriInfo().uri())
                .size(fetchedItems.size())
                .build();

        if (logger.isDebugEnabled()) {
            logger.debug("Successfully retrieved collection: {}", exchange.getRequest().getUrl());
        }

        return exchange.response(Response.<Collection<T>>create(StatusCode._200_OK)
                .body(collection));
    }

    private int getOffset(Exchange<Collection<T>> exchange) {
        return Optional.ofNullable(exchange.getRequest().getApiUriInfo().offset())
                .orElse(DEFAULT_OFFSET);
    }

    private int getLimit(Exchange<Collection<T>> exchange) {
        return Optional.ofNullable(exchange.getRequest().getApiUriInfo().limit())
                .orElse(config.getDefaultGetCollectionLimit());
    }
}
