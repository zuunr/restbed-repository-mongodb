/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.repository.mongodb.config;

import javax.validation.constraints.NotEmpty;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.validation.annotation.Validated;

/**
 * <p>This class makes sure that any spring mongodb property,
 * not validated by spring, is validated when running.</p>
 * 
 * <p>Properties in this class should not be used.</p>
 * 
 * @author Mikael Ahlberg
 */
@ConfigurationProperties(prefix = "spring.data.mongodb")
@Validated
class MongoDbPropertyValidator {

    @NotEmpty(message = "You must set the mongodb uri")
    private final String uri;
    
    @ConstructorBinding
    public MongoDbPropertyValidator(String uri) {
        this.uri = uri;
    }
}
