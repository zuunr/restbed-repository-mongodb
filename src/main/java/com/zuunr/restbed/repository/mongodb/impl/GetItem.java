/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.repository.mongodb.impl;

import com.zuunr.json.JsonObjectSupport;
import com.zuunr.restbed.core.exchange.Exchange;
import com.zuunr.restbed.core.exchange.Response;
import com.zuunr.restbed.core.exchange.StatusCode;
import com.zuunr.restbed.repository.ReactiveRepository;
import com.zuunr.restbed.repository.mongodb.util.CollectionNameProvider;
import com.zuunr.restbed.repository.mongodb.util.ResourceDeserializer;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

/**
 * <p>Primary getItem implementation of {@link ReactiveRepository}.</p>
 * 
 * @see ReactiveRepository
 */
@Component
public class GetItem<T extends JsonObjectSupport> {

    private final Logger logger = LoggerFactory.getLogger(GetItem.class);

    private static final String MONGO_INTERNAL_ID_PARAM = "_id";

    private ReactiveMongoTemplate reactiveMongoTemplate;
    private CollectionNameProvider collectionNameProvider;
    private ResourceDeserializer resourceDeserializer;

    @Autowired
    public GetItem(
            ReactiveMongoTemplate reactiveMongoTemplate,
            CollectionNameProvider collectionNameProvider,
            ResourceDeserializer resourceDeserializer) {
        this.reactiveMongoTemplate = reactiveMongoTemplate;
        this.collectionNameProvider = collectionNameProvider;
        this.resourceDeserializer = resourceDeserializer;
    }

    /**
     * @see ReactiveRepository#getItem(Exchange, Class)
     */
    public Mono<Exchange<T>> getItem(Exchange<T> exchange, Class<T> resourceClass) {
        String collectionName = collectionNameProvider.getCollectionName(exchange.getRequest());
        String coreId = exchange.getRequest().getApiUriInfo().coreId();
        
        ObjectId objectId;
        try {
            objectId = new ObjectId(coreId);
        } catch(IllegalArgumentException e) {
            return Mono.just(emptyGet(exchange));
        }

        Query query = new Query()
                .addCriteria(Criteria
                        .where(MONGO_INTERNAL_ID_PARAM)
                        .is(objectId));

        return reactiveMongoTemplate.findOne(query, Document.class, collectionName)
                .flatMap(o -> successGet(exchange, o, resourceClass))
                .defaultIfEmpty(emptyGet(exchange));
    }

    private Exchange<T> emptyGet(Exchange<T> originalExchange) {
        return originalExchange.response(Response.<T>create(StatusCode._404_NOT_FOUND));
    }

    private Mono<Exchange<T>> successGet(Exchange<T> exchange, Document fetchedDocument, Class<T> resourceClass) {
        fetchedDocument.remove(MONGO_INTERNAL_ID_PARAM);
        T body = resourceDeserializer.deserialize(fetchedDocument, resourceClass);

        if (logger.isDebugEnabled()) {
            logger.debug("Successfully retrieved item: {}", exchange.getRequest().getUrl());
        }

        return Mono.just(exchange
                .response(Response.<T>create(StatusCode._200_OK)
                        .body(body)));
    }
}
