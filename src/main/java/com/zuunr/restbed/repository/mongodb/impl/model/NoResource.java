/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.repository.mongodb.impl.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectSupport;

/**
 * <p>NoResource is an empty model implementing JsonObjectSupport.</p>
 * 
 * <p>It uses {@link NoResourceDeserializer} for deserializing ({@link JsonDeserialize}).</p>
 * 
 * @author Mikael Ahlberg
 */
@JsonDeserialize(using = NoResourceDeserializer.class)
public class NoResource implements JsonObjectSupport {

    public static final NoResource EMPTY = new NoResource();

    private NoResource() {}

    @Override
    public JsonObject asJsonObject() {
        return JsonObject.EMPTY;
    }
}
