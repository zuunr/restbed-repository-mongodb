/*
 * Copyright 2019 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.repository.mongodb.filter;

import java.util.regex.Matcher;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;

/**
 * <p>The FilterQueryProvider is responsible for translating filter techniques 
 * used in API to a spring data friendly filter.</p>
 * 
 * <p>The implementation supports the following 6 API filtering techniques:</p>
 * 
 * <ul>
 * <li>myParam={value1} - Where my parameter must be equal to {value1}</li>
 * <li>myParam={value1},{value2}... - Where my parameter must be equal to any of the provided values</li>
 * <li>myParam=[{value1},{value2}] - Where my parameter must be greater or equals
 *  to {value1} and less than or equals to {value2}</li>
 * <li>myParam=({value1},{value2}) - Where my parameter must be greater than
 *  {value1} and less than {value2}</li>
 * <li>myParam=[{value1},{value2}) - Where my parameter must be greater or equals
 *  to {value1} and less than {value2}</li>
 * <li>myParam=({value1},{value2}] - Where my parameter must be greater than
 *  {value1} and less than or equals to {value2}</li>
 * </ul>
 * 
 * <p>A parameter must only be added once, e.g. only one of the above techniques
 * must be applied.</p>
 * 
 * @author Niklas Eldberger
 * @author Mikael Ahlberg
 */
@Component
public class FilterQueryProvider {
    
    private RangeCriteriaCreator rangeCriteriaCreator;
    private FixedValueCriteriaCreator fixedValueCriteriaCreator;
    
    @Autowired
    public FilterQueryProvider(
            RangeCriteriaCreator rangeCriteriaCreator,
            FixedValueCriteriaCreator fixedValueCriteriaCreator) {
        this.rangeCriteriaCreator = rangeCriteriaCreator;
        this.fixedValueCriteriaCreator = fixedValueCriteriaCreator;
    }

    /**
     * <p>Creates a {@link Query} object given the filter parameters, which could be
     * used to filter out elements not matching the selected filter.</p>
     * 
     * @param filter is an {@link JsonArray} containing the different filters
     * @return a new Query object
     */
    public Query getFilterQuery(JsonArray filter) {
        Query query = new Query();

        for (JsonObject filterObject : filter.asList(JsonObject.class)) {
            String path = filterObject.get("path").getValue(String.class);
            String expression = filterObject.get("expr", "").getValue(String.class);

            query.addCriteria(createCriteria(path, expression));
        }

        return query;
    }

    private Criteria createCriteria(String path, String filterExpression) {
        Matcher filterRangeMatcher = RangeCriteriaCreator.pattern.matcher(filterExpression);

        if (filterRangeMatcher.matches()) {
            return rangeCriteriaCreator.createCriteria(filterRangeMatcher, path);
        }
        
        return fixedValueCriteriaCreator.createCriteria(filterExpression, path);
    }
}
