/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.repository.mongodb.impl;

import java.util.Map;

import org.bson.Document;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

import com.mongodb.client.result.UpdateResult;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectSupport;
import com.zuunr.restbed.core.exchange.ErrorBody;
import com.zuunr.restbed.core.exchange.Exchange;
import com.zuunr.restbed.core.exchange.Method;
import com.zuunr.restbed.core.exchange.Request;
import com.zuunr.restbed.core.exchange.Response;
import com.zuunr.restbed.core.exchange.StatusCode;
import com.zuunr.restbed.core.service.util.EtagProvider;
import com.zuunr.restbed.repository.ReactiveRepository;
import com.zuunr.restbed.repository.mongodb.impl.model.NoResource;
import com.zuunr.restbed.repository.mongodb.util.CollectionNameProvider;
import com.zuunr.restbed.repository.mongodb.util.ResourceDeserializer;

import reactor.core.publisher.Mono;

/**
 * <p>Primary updateItem implementation of {@link ReactiveRepository}.</p>
 * 
 * @see ReactiveRepository
 */
@Component
public class UpdateItem<T extends JsonObjectSupport> {

    private final Logger logger = LoggerFactory.getLogger(UpdateItem.class);

    private static final String MONGO_INTERNAL_ID_PARAM = "_id";

    private static final String META_ETAG_PARAM = "_internal.etag";
    private static final String CONFLICT_ERROR_MESSAGE = "Simultaneous update";

    private ReactiveMongoTemplate reactiveMongoTemplate;
    private EtagProvider etagProvider;
    private CollectionNameProvider collectionNameProvider;
    private GetItem<NoResource> getItem;
    private ResourceDeserializer resourceDeserializer;

    @Autowired
    public UpdateItem(
            ReactiveMongoTemplate reactiveMongoTemplate,
            EtagProvider etagProvider,
            CollectionNameProvider collectionNameProvider,
            GetItem<NoResource> getItem,
            ResourceDeserializer resourceDeserializer) {
        this.reactiveMongoTemplate = reactiveMongoTemplate;
        this.etagProvider = etagProvider;
        this.collectionNameProvider = collectionNameProvider;
        this.getItem = getItem;
        this.resourceDeserializer = resourceDeserializer;
    }

    /**
     * @see ReactiveRepository#updateItem(Exchange, Class)
     */
    public Mono<Exchange<T>> updateItem(Exchange<T> exchange, Class<T> resourceClass) {
        String collectionName = collectionNameProvider.getCollectionName(exchange.getRequest());
        String coreId = exchange.getRequest().getApiUriInfo().coreId();

        JsonObject body = exchange.getRequest().getBodyAsJsonObject();
        String etag = etagProvider.getEtag(body);
        JsonObject bodyWithNewEtag = etagProvider.appendEtag(body);
        
        ObjectId objectId;
        try {
            objectId = new ObjectId(coreId);
        } catch(IllegalArgumentException e) {
            return Mono.just(exchange.response(Response.<T>create(StatusCode._404_NOT_FOUND)));
        }

        Query query = Query.query(Criteria
                .where(MONGO_INTERNAL_ID_PARAM)
                    .is(objectId)
                .and(META_ETAG_PARAM)
                    .is(etag));

        return reactiveMongoTemplate.updateFirst(query, createUpdateDocumentFromBody(bodyWithNewEtag), collectionName)
                .flatMap(o -> successUpdate(exchange, bodyWithNewEtag, o, resourceClass));
    }

    private Update createUpdateDocumentFromBody(JsonObject body) {
        Map<String, Object> translatedBody = body.asMapsAndLists();

        return Update.fromDocument(new Document(translatedBody));
    }

    private Mono<Exchange<T>> successUpdate(Exchange<T> exchange, JsonObject savedBody, UpdateResult updateResult, Class<T> resourceClass) {
        if (updateResult.getModifiedCount() == 0) {
            return checkIfExistsAndReturnResponse(exchange);
        }

        if (logger.isDebugEnabled()) {
            logger.debug("Successfully updated item: {}", exchange.getRequest().getUrl());
        }

        T body = resourceDeserializer.deserializeJsonObject(savedBody, resourceClass);

        return Mono.just(exchange
                .response(Response.<T>create(StatusCode._200_OK)
                        .body(body)));
    }

    private Mono<Exchange<T>> checkIfExistsAndReturnResponse(Exchange<T> exchange) {
        Exchange<NoResource> fetchExchange = Exchange.<NoResource>empty()
                .request(Request.<NoResource>create(Method.GET)
                        .url(exchange.getRequest().getUrl()));

        return getItem.getItem(fetchExchange, NoResource.class)
                .map(o -> returnStatusCode(exchange, o));
    }

    private Exchange<T> returnStatusCode(Exchange<T> originalExchange, Exchange<NoResource> fetchExchange) {
        Response<T> response = null;

        if (fetchExchange.getResponse().getStatusCode() == StatusCode._200_OK.getCode()) {
            response = Response.<T>create(StatusCode._409_CONFLICT)
                    .errorBody(ErrorBody.of(JsonObject.EMPTY.put("error", CONFLICT_ERROR_MESSAGE)));
        } else {
            response = Response.<T>create(StatusCode._404_NOT_FOUND);
        }

        if (logger.isDebugEnabled()) {
            logger.debug("Failed to update item, returning with status code: {}", response.getStatusCode());
        }

        return originalExchange.response(response);
    }
}
