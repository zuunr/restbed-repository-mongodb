/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.repository.mongodb.filter;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.regex.Matcher;

import org.bson.Document;
import org.junit.jupiter.api.Test;
import org.springframework.data.mongodb.core.query.Criteria;

class RangeCriteriaCreatorTest {

    private RangeCriteriaCreator rangeCriteriaCreator = new RangeCriteriaCreator();

    @Test
    void givenRangeValuesShouldCreateCriteria() {
        Criteria filterCriteria = rangeCriteriaCreator.createCriteria(initMatcher("(10,42)"), "myParam");
        Document queryDocument = filterCriteria.getCriteriaObject();

        assertEquals("10", ((Document) queryDocument.get("myParam")).get("$gt"));
        assertEquals("42", ((Document) queryDocument.get("myParam")).get("$lt"));
        assertEquals(2, ((Document) queryDocument.get("myParam")).size());
    }

    @Test
    void givenInclusiveRangeValuesShouldCreateCriteria() {
        Criteria filterCriteria = rangeCriteriaCreator.createCriteria(initMatcher("[10,42]"), "myParam");
        Document queryDocument = filterCriteria.getCriteriaObject();

        assertEquals("10", ((Document) queryDocument.get("myParam")).get("$gte"));
        assertEquals("42", ((Document) queryDocument.get("myParam")).get("$lte"));
        assertEquals(2, ((Document) queryDocument.get("myParam")).size());
    }

    @Test
    void givenOneRangeValueShouldCreateCriteria() {
        Criteria filterCriteria = rangeCriteriaCreator.createCriteria(initMatcher("(10,)"), "myParam");
        Document queryDocument = filterCriteria.getCriteriaObject();

        assertEquals("10", ((Document) queryDocument.get("myParam")).get("$gt"));
        assertEquals(1, ((Document) queryDocument.get("myParam")).size());
    }

    @Test
    void givenOneInclusiveRangeValueShouldCreateCriteria() {
        Criteria filterCriteria = rangeCriteriaCreator.createCriteria(initMatcher("[10,]"), "myParam");
        Document queryDocument = filterCriteria.getCriteriaObject();

        assertEquals("10", ((Document) queryDocument.get("myParam")).get("$gte"));
        assertEquals(1, ((Document) queryDocument.get("myParam")).size());
    }

    @Test
    void givenSecondRangeValueShouldCreateCriteria() {
        Criteria filterCriteria = rangeCriteriaCreator.createCriteria(initMatcher("(,42)"), "myParam");
        Document queryDocument = filterCriteria.getCriteriaObject();

        assertEquals("42", ((Document) queryDocument.get("myParam")).get("$lt"));
        assertEquals(1, ((Document) queryDocument.get("myParam")).size());
    }

    @Test
    void givenSecondInclusiveRangeValueShouldCreateCriteria() {
        Criteria filterCriteria = rangeCriteriaCreator.createCriteria(initMatcher("[,42]"), "myParam");
        Document queryDocument = filterCriteria.getCriteriaObject();

        assertEquals("42", ((Document) queryDocument.get("myParam")).get("$lte"));
        assertEquals(1, ((Document) queryDocument.get("myParam")).size());
    }
    
    private Matcher initMatcher(String regex) {
        Matcher matcher = RangeCriteriaCreator.pattern.matcher(regex);
        matcher.matches();
        
        return matcher;
    }
}
