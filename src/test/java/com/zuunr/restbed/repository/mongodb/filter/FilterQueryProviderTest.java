/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.repository.mongodb.filter;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.regex.MatchResult;

import org.bson.Document;
import org.junit.jupiter.api.Test;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;

class FilterQueryProviderTest {

    private FilterQueryProvider filterCriteriaProvider = new FilterQueryProvider(new MyRangeCriteriaCreator(), new MyFixedValueCriteriaCreator());
    
    private final Criteria myRangeCriteria = new Criteria("rangeKey").is("rangeValue");
    private final Criteria myFixedValueCriteria = new Criteria("fixedKey").is("fixedValue");

    @Test
    void givenNoFilterShouldReturnEmptyQuery() {
        Query filterQuery = filterCriteriaProvider.getFilterQuery(JsonArray.EMPTY);
        Document queryDocument = filterQuery.getQueryObject();

        assertEquals(0, queryDocument.size());
    }

    @Test
    void givenRangeValuesShouldCreateQuery() {
        JsonObject filter = JsonObject.EMPTY
                .put("path", "myParam")
                .put("expr", "(10,42)");

        Query filterQuery = filterCriteriaProvider.getFilterQuery(JsonArray.of(filter));
        Document queryDocument = filterQuery.getQueryObject();

        assertEquals("rangeValue", queryDocument.get("rangeKey"));
    }

    @Test
    void givenInclusiveRangeValuesShouldCreateQuery() {
        JsonObject filter = JsonObject.EMPTY
                .put("path", "myParam")
                .put("expr", "[10,42]");

        Query filterQuery = filterCriteriaProvider.getFilterQuery(JsonArray.of(filter));
        Document queryDocument = filterQuery.getQueryObject();

        assertEquals("rangeValue", queryDocument.get("rangeKey"));
    }

    @Test
    void givenOneRangeValueShouldCreateQuery() {
        JsonObject filter = JsonObject.EMPTY
                .put("path", "myParam")
                .put("expr", "(10,)");

        Query filterQuery = filterCriteriaProvider.getFilterQuery(JsonArray.of(filter));
        Document queryDocument = filterQuery.getQueryObject();

        assertEquals("rangeValue", queryDocument.get("rangeKey"));
    }

    @Test
    void givenOneInclusiveRangeValueShouldCreateQuery() {
        JsonObject filter = JsonObject.EMPTY
                .put("path", "myParam")
                .put("expr", "[10,]");

        Query filterQuery = filterCriteriaProvider.getFilterQuery(JsonArray.of(filter));
        Document queryDocument = filterQuery.getQueryObject();

        assertEquals("rangeValue", queryDocument.get("rangeKey"));
    }

    @Test
    void givenSecondRangeValueShouldCreateQuery() {
        JsonObject filter = JsonObject.EMPTY
                .put("path", "myParam")
                .put("expr", "(,42)");

        Query filterQuery = filterCriteriaProvider.getFilterQuery(JsonArray.of(filter));
        Document queryDocument = filterQuery.getQueryObject();

        assertEquals("rangeValue", queryDocument.get("rangeKey"));
    }

    @Test
    void givenSecondInclusiveRangeValueShouldCreateQuery() {
        JsonObject filter = JsonObject.EMPTY
                .put("path", "myParam")
                .put("expr", "[,42]");

        Query filterQuery = filterCriteriaProvider.getFilterQuery(JsonArray.of(filter));
        Document queryDocument = filterQuery.getQueryObject();

        assertEquals("rangeValue", queryDocument.get("rangeKey"));
    }
    
    @Test
    void givenFixedValueShouldCreateQuery() {
        JsonObject filter = JsonObject.EMPTY
                .put("path", "myParam")
                .put("expr", "42");

        Query filterQuery = filterCriteriaProvider.getFilterQuery(JsonArray.of(filter));
        Document queryDocument = filterQuery.getQueryObject();

        assertEquals("fixedValue", queryDocument.get("fixedKey"));
    }
    
    @Test
    void givenFixedValuesShouldCreateQuery() {
        JsonObject filter = JsonObject.EMPTY
                .put("path", "myParam")
                .put("expr", "42,10,30");

        Query filterQuery = filterCriteriaProvider.getFilterQuery(JsonArray.of(filter));
        Document queryDocument = filterQuery.getQueryObject();

        assertEquals("fixedValue", queryDocument.get("fixedKey"));
    }
    
    @Test
    void givenMultipleParametersShouldCreateQuery() {
        JsonObject filter1 = JsonObject.EMPTY
                .put("path", "myParam")
                .put("expr", "42,10,30");
        JsonObject filter2 = JsonObject.EMPTY
                .put("path", "myOtherParam")
                .put("expr", "[,42]");

        Query filterQuery = filterCriteriaProvider.getFilterQuery(JsonArray.of(filter1, filter2));
        Document queryDocument = filterQuery.getQueryObject();

        assertEquals("rangeValue", queryDocument.get("rangeKey"));
        assertEquals("fixedValue", queryDocument.get("fixedKey"));
    }
    
    private class MyRangeCriteriaCreator extends RangeCriteriaCreator {
        @Override
        public Criteria createCriteria(MatchResult matcher, String path) {
            return myRangeCriteria;
        }
    }
    
    private class MyFixedValueCriteriaCreator extends FixedValueCriteriaCreator {
        @Override
        public Criteria createCriteria(String filterExpression, String path) {
            return myFixedValueCriteria;
        }
    }
}
