/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.repository.mongodb;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.util.JsonObjectWrapper;
import com.zuunr.restbed.core.api.ApiUriInfo;
import com.zuunr.restbed.core.exchange.Collection;
import com.zuunr.restbed.core.exchange.Exchange;
import com.zuunr.restbed.core.exchange.Method;
import com.zuunr.restbed.core.exchange.Request;
import com.zuunr.restbed.core.exchange.Response;
import com.zuunr.restbed.core.exchange.StatusCode;
import com.zuunr.restbed.repository.ReactiveRepository;
import com.zuunr.restbed.repository.mongodb.example.resource.Account;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@SpringBootApplication(scanBasePackages = {"com.zuunr.restbed.repository.mongodb", "com.zuunr.restbed.core.service.util"})
class ReactiveMongoRepositoryIT {

    private @Autowired ReactiveRepository<JsonObjectWrapper> reactiveRepository;
    private @Autowired ReactiveRepository<Account> accountReactiveRepository;

    @Test
    void saveAndGetItem() {
        final String id = ApiUriInfo.createItemId();

        // SaveItem
        Exchange<JsonObjectWrapper> postExchange = Exchange.<JsonObjectWrapper>empty()
                .request(Request.<JsonObjectWrapper>create(Method.POST)
                        .url("http://somedomain.com/v1/myapi/api/thecollection")
                        .body(JsonObjectWrapper.of(mockRequest(id))));

        postExchange = reactiveRepository.saveItem(postExchange, JsonObjectWrapper.class).block();
        Response<JsonObjectWrapper> postResponse = postExchange.getResponse();

        assertNotNull(postResponse);
        assertEquals(StatusCode._201_CREATED.getCode(), (int) postResponse.getStatusCode());

        // GetItem
        Exchange<JsonObjectWrapper> getExchange = Exchange.<JsonObjectWrapper>empty()
                .request(Request.<JsonObjectWrapper>create(Method.GET)
                        .url("http://somedomain.com/v1/myapi/api/thecollection/" + id));

        getExchange = reactiveRepository.getItem(getExchange, JsonObjectWrapper.class).block();
        Response<JsonObjectWrapper> getResponse = getExchange.getResponse();

        assertNotNull(getResponse);
        assertEquals(StatusCode._200_OK.getCode(), (int) getResponse.getStatusCode());
    }
    
    @Test
    void getNonExistingItemShouldReturn404() {
        final String id = UUID.randomUUID().toString();

        // GetItem
        Exchange<JsonObjectWrapper> getExchange = Exchange.<JsonObjectWrapper>empty()
                .request(Request.<JsonObjectWrapper>create(Method.GET)
                        .url("http://somedomain.com/v1/myapi/api/thecollection/" + id));

        getExchange = reactiveRepository.getItem(getExchange, JsonObjectWrapper.class).block();
        Response<JsonObjectWrapper> getResponse = getExchange.getResponse();

        assertNotNull(getResponse);
        assertEquals(StatusCode._404_NOT_FOUND.getCode(), (int) getResponse.getStatusCode());
    }

    @Test
    void deleteNonExistingItemShouldReturn404() {
        final String id = UUID.randomUUID().toString();

        // DeleteItem
        Exchange<JsonObjectWrapper> getExchange = Exchange.<JsonObjectWrapper>empty()
                .request(Request.<JsonObjectWrapper>create(Method.DELETE)
                        .url("http://somedomain.com/v1/myapi/api/thecollection/" + id)
                        .body(JsonObjectWrapper.of(JsonObject.EMPTY.put(JsonArray.of("_internal", "etag"), "12345"))));

        getExchange = reactiveRepository.deleteItem(getExchange).block();
        Response<JsonObjectWrapper> getResponse = getExchange.getResponse();

        assertNotNull(getResponse);
        assertEquals(StatusCode._404_NOT_FOUND.getCode(), (int) getResponse.getStatusCode());
    }

    @Test
    void getCollectionWithItems() {
        // GetItems
        Exchange<Collection<JsonObjectWrapper>> getExchange = Exchange.<Collection<JsonObjectWrapper>>empty()
                .request(Request.<Collection<JsonObjectWrapper>>create(Method.GET)
                        .url("http://somedomain.com/v1/myapi/api/thecollection"));

        getExchange = reactiveRepository.getCollection(getExchange, JsonObjectWrapper.class).block();
        Response<Collection<JsonObjectWrapper>> getResponse = getExchange.getResponse();

        assertNotNull(getResponse);
        assertEquals(StatusCode._200_OK.getCode(), (int) getResponse.getStatusCode());
    }

    @Test
    void saveAndGetAccountItem() {
        final String id = ApiUriInfo.createItemId();

        // SaveItem
        Exchange<Account> postExchange = Exchange.<Account>empty()
                .request(Request.<Account>create(Method.POST)
                        .url("http://somedomain.com/v1/myapi/api/thecollection")
                        .body(mockAccountRequest(id)));

        postExchange = accountReactiveRepository.saveItem(postExchange, Account.class).block();
        Response<Account> postResponse = postExchange.getResponse();

        assertNotNull(postResponse);
        assertEquals(StatusCode._201_CREATED.getCode(), (int) postResponse.getStatusCode());

        // GetItem
        Exchange<Account> getExchange = Exchange.<Account>empty()
                .request(Request.<Account>create(Method.GET)
                        .url("http://somedomain.com/v1/myapi/api/thecollection/" + id));

        Exchange<Account> savedExchange = accountReactiveRepository.getItem(getExchange, Account.class).block();
        Response<Account> getResponse = savedExchange.getResponse();

        assertNotNull(getResponse);
        assertEquals(StatusCode._200_OK.getCode(), (int) getResponse.getStatusCode());
    }

    private JsonObject mockRequest(String id) {
        return JsonObject.EMPTY
                .put(JsonArray.of("meta", "createdAt"), "2018-08-21T14:46:48.296Z")
                .put(JsonArray.of("meta", "modifiedAt"), "2018-08-21T14:46:48.296Z")
                .put("name", "Laura Andersson")
                .put("id", id)
                .put("href", "http://localhost:8080/v1/2018-08-21T14-46-46-218Z-MyExampleIT/samples/accounts/" + id)
                .put("status", "ACTIVE");
    }

    private Account mockAccountRequest(String id) {
        Account account = new Account();
        account.setId(id);
        account.setAccountNumber("12345");

        return account;
    }
}
