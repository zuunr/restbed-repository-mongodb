/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.repository.mongodb.example.resource;

import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectSupport;

public class Account implements JsonObjectSupport {

    private Internal _internal;
    private String id;
    private String accountNumber;

    public Internal get_internal() {
        return _internal;
    }

    public void set_internal(Internal _internal) {
        this._internal = _internal;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    @Override
    public JsonObject asJsonObject() {
        return JsonObject.EMPTY
                .put("id", id)
                .put("accountNumber", accountNumber);
    }
}
