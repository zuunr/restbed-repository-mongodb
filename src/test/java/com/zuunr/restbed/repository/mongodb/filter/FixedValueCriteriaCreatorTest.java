/*
 * Copyright 2019 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.repository.mongodb.filter;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.bson.Document;
import org.junit.jupiter.api.Test;
import org.springframework.data.mongodb.core.query.Criteria;

import com.mongodb.BasicDBList;

class FixedValueCriteriaCreatorTest {
    
    private FixedValueCriteriaCreator fixedValueCriteriaCreator = new FixedValueCriteriaCreator();
    
    @Test
    void givenFixedValueShouldCreateCriteria() {
        Criteria filterCriteria = fixedValueCriteriaCreator.createCriteria("42", "myParam");
        Document queryDocument = filterCriteria.getCriteriaObject();

        assertEquals("42", queryDocument.get("myParam"));
    }
    
    @Test
    void givenFixedValuesShouldCreateCriteria() {
        Criteria filterCriteria = fixedValueCriteriaCreator.createCriteria("42,100,30,25", "myParam");
        Document queryDocument = filterCriteria.getCriteriaObject();

        assertEquals(4, ((BasicDBList) queryDocument.get("$or")).size());
        
        Set<String> valuesToMatch = new HashSet<>(Arrays.asList("42", "100", "30", "25"));
        
        for (Object documentObject : ((BasicDBList) queryDocument.get("$or"))) {
            Document document = (Document) documentObject;
            
            valuesToMatch.remove(document.get("myParam"));
        }
        
        assertTrue(valuesToMatch.isEmpty());
    }
}
