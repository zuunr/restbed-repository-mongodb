/*
 * Copyright 2019 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.repository.mongodb.config;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.math.BigDecimal;

import org.bson.types.Decimal128;
import org.junit.jupiter.api.Test;
import org.springframework.data.mongodb.core.convert.MongoCustomConversions;

import com.zuunr.restbed.repository.mongodb.config.MongoConverterConfig.BigDecimalDecimal128Converter;

class MongoConverterConfigTest {
    
    private MongoConverterConfig mongoConverterConfig = new MongoConverterConfig();
    
    @Test
    void shouldRegisterDecimal128Converter() {
        MongoCustomConversions mongoCustomConversions = mongoConverterConfig.mongoCustomConversions();
        
        assertTrue(mongoCustomConversions.hasCustomWriteTarget(BigDecimal.class, Decimal128.class));
    }
    
    @Test
    void shouldConvertBigDecimalToDecimal128() {
        BigDecimalDecimal128Converter bigDecimalDecimal128Converter = mongoConverterConfig.new BigDecimalDecimal128Converter();
        
        final BigDecimal decimalToConvert = new BigDecimal("150.00");
        
        Decimal128 convertedDecimal = bigDecimalDecimal128Converter.convert(decimalToConvert);
        
        assertEquals(decimalToConvert, convertedDecimal.bigDecimalValue());
    }
}
