/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.repository.mongodb.sort;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.data.domain.Sort.Order;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;

class SortOrderProviderTest {

    private SortOrderProvider sortOrderProvider = new SortOrderProvider();

    @Test
    void givenNoSortOrderShouldReturnEmptyOrderList() {
        List<Order> sortOrder = sortOrderProvider.getSortOrder(JsonArray.EMPTY);

        assertTrue(sortOrder.isEmpty());
    }

    @Test
    void givenOneParameterAscendingShouldCreateAscendingOrder() {
        JsonObject parameter = JsonObject.EMPTY
                .put("param", "myParam")
                .put("asc", JsonValue.TRUE);

        List<Order> sortOrder = sortOrderProvider.getSortOrder(JsonArray.of(parameter));

        assertEquals("myParam", sortOrder.get(0).getProperty());
        assertTrue(sortOrder.get(0).isAscending());
    }

    @Test
    void givenOneParameterDescendingShouldCreateDescendingOrder() {
        JsonObject parameter = JsonObject.EMPTY
                .put("param", "myParam")
                .put("asc", JsonValue.FALSE);

        List<Order> sortOrder = sortOrderProvider.getSortOrder(JsonArray.of(parameter));

        assertEquals("myParam", sortOrder.get(0).getProperty());
        assertTrue(sortOrder.get(0).isDescending());
    }

    @Test
    void givenOneParameterWithNoOrderShouldCreateAscendingOrder() {
        JsonObject parameter = JsonObject.EMPTY
                .put("param", "myParam");

        List<Order> sortOrder = sortOrderProvider.getSortOrder(JsonArray.of(parameter));

        assertEquals("myParam", sortOrder.get(0).getProperty());
        assertTrue(sortOrder.get(0).isAscending());
    }

    @Test
    void givenMultiParametersShouldCreateOrder() {
        JsonObject parameter1 = JsonObject.EMPTY
                .put("param", "myParam1");
        JsonObject parameter2 = JsonObject.EMPTY
                .put("param", "myParam2")
                .put("asc", JsonValue.FALSE);

        List<Order> sortOrder = sortOrderProvider.getSortOrder(JsonArray.of(parameter1, parameter2));

        for (Order order : sortOrder) {
            if (order.getProperty().equals("myParam1")) {
                assertTrue(order.isAscending());
            } else if (order.getProperty().equals("myParam2")) {
                assertTrue(order.isDescending());
            } else {
                fail("Unknown property was added");
            }
        }
    }
}
