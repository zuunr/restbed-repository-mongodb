/*
 * Copyright 2019 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.repository.mongodb.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.BigInteger;

import org.bson.types.Decimal128;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.Base64Variant;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonStreamContext;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.core.SerializableString;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.core.Version;

class Decimal128SerializerTest {
    
    private Decimal128Serializer decimal128Serializer = new Decimal128Serializer();
    
    @Test
    void given() throws IOException {
        final BigDecimal myBigDecimal = new BigDecimal("120.00");
        final Decimal128 myDecimal128 = new Decimal128(myBigDecimal);
        MyJsonGenerator myJsonGenerator = new MyJsonGenerator();
        
        decimal128Serializer.serialize(myDecimal128, myJsonGenerator, null);
        
        assertEquals(myBigDecimal, myJsonGenerator.value);
    }
    
    private class MyJsonGenerator extends JsonGenerator {
        
        public BigDecimal value; 

        @Override
        public JsonGenerator setCodec(ObjectCodec oc) {
            return null;
        }
        @Override
        public ObjectCodec getCodec() {
            return null;
        }
        @Override
        public Version version() {
            return null;
        }
        @Override
        public JsonGenerator enable(Feature f) {
            return null;
        }
        @Override
        public JsonGenerator disable(Feature f) {
            return null;
        }
        @Override
        public boolean isEnabled(Feature f) {
            return false;
        }
        @Override
        public int getFeatureMask() {
            return 0;
        }
        @Override
        public JsonGenerator setFeatureMask(int values) {
            return null;
        }
        @Override
        public JsonGenerator useDefaultPrettyPrinter() {
            return null;
        }
        @Override
        public void writeStartArray() throws IOException {
        }
        @Override
        public void writeEndArray() throws IOException {
        }
        @Override
        public void writeStartObject() throws IOException {
        }
        @Override
        public void writeEndObject() throws IOException {
        }
        @Override
        public void writeFieldName(String name) throws IOException {
        }
        @Override
        public void writeFieldName(SerializableString name) throws IOException {
        }
        @Override
        public void writeString(String text) throws IOException {
        }
        @Override
        public void writeString(char[] text, int offset, int len) throws IOException {
        }
        @Override
        public void writeString(SerializableString text) throws IOException {
        }
        @Override
        public void writeRawUTF8String(byte[] text, int offset, int length) throws IOException {
        }
        @Override
        public void writeUTF8String(byte[] text, int offset, int length) throws IOException {
        }
        @Override
        public void writeRaw(String text) throws IOException {
        }
        @Override
        public void writeRaw(String text, int offset, int len) throws IOException {
        }
        @Override
        public void writeRaw(char[] text, int offset, int len) throws IOException {
        }
        @Override
        public void writeRaw(char c) throws IOException {
        }
        @Override
        public void writeRawValue(String text) throws IOException {
        }
        @Override
        public void writeRawValue(String text, int offset, int len) throws IOException {
        }
        @Override
        public void writeRawValue(char[] text, int offset, int len) throws IOException {
        }
        @Override
        public void writeBinary(Base64Variant bv, byte[] data, int offset, int len) throws IOException {
        }
        @Override
        public int writeBinary(Base64Variant bv, InputStream data, int dataLength) throws IOException {
            return 0;
        }
        @Override
        public void writeNumber(int v) throws IOException {
        }
        @Override
        public void writeNumber(long v) throws IOException {
        }
        @Override
        public void writeNumber(BigInteger v) throws IOException {
        }
        @Override
        public void writeNumber(double v) throws IOException {
        }
        @Override
        public void writeNumber(float v) throws IOException {
        }
        @Override
        public void writeNumber(BigDecimal v) throws IOException {
            value = v;
        }
        @Override
        public void writeNumber(String encodedValue) throws IOException {
        }
        @Override
        public void writeBoolean(boolean state) throws IOException {
        }
        @Override
        public void writeNull() throws IOException {
        }
        @Override
        public void writeObject(Object pojo) throws IOException {
        }
        @Override
        public void writeTree(TreeNode rootNode) throws IOException {
        }
        @Override
        public JsonStreamContext getOutputContext() {
            return null;
        }
        @Override
        public void flush() throws IOException {
        }
        @Override
        public boolean isClosed() {
            return false;
        }
        @Override
        public void close() throws IOException {   
        }
    }
}
